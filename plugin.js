CKEDITOR.plugins.add( 'filemanager', {
    requires: 'iframedialog',
    icons: 'filemanager',
    init: function( editor ) {
        editor.addCommand( 'filemanager', new CKEDITOR.dialogCommand( 'filemanagerDialog' ) );
        editor.ui.addButton( 'Filemanager', {
            label: 'Insert Filemanager',
            command: 'filemanager',
            toolbar: 'insert'
        });

        CKEDITOR.dialog.add( 'filemanagerDialog', this.path + 'dialogs/filemanager.js' );
    }
});