CKEDITOR.dialog.add( 'filemanagerDialog', function( editor ) {
    return {
        title: 'Filemanager Properties',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'info',
                label: 'File Settings',
                elements: [
                    {
                        id: 'txtUrl',
					    type: 'text',
						label: editor.lang.common.url,
						required: true,
                        validate: CKEDITOR.dialog.validate.notEmpty( editor.lang.image.urlMissing )
                    },
                    {
                        type: 'button',
                        id: 'browse',
                        label: editor.lang.common.browseServer,
                        hidden: true,
                        filebrowser: 'info:txtUrl'
                    }
                ]
            }
        ],
        onOk: function() {
            var dialog = this;
            editor.insertHtml( dialog.getValueOf( 'info', 'txtUrl' ) );
        }
    };
});