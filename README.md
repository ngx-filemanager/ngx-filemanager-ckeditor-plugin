This project requires http://ckeditor.com/addon/filebrowser 

# CKEditor Basic Configuration

In order to integrate CKEditor with a file manager, you need to set the following configuration options:

The config.filebrowserBrowseUrl setting contains the location of an external file browser that should be launched when the Browse Server button is pressed.
The config.filebrowserUploadUrl setting contains the location of a script that handles file uploads. If set, the Upload tab will appear in some dialog windows — the ones where such functionality is available, i.e. Link, Image and Flash Properties.
The sample below shows basic configuration code that can be used to create a CKEditor instance with the file manager configured.

```
CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl: '/browser/browse.php',
    filebrowserUploadUrl: '/uploader/upload.php'
});
```

In order to integrate with ng2-filemanger you have the query parameter ?editor=CKEditor to the filebrowserBrowseUrl like this:

```
CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl: '/browser/browse.php?editor=CKEditor',
    filebrowserUploadUrl: '/uploader/upload.php'
});
```